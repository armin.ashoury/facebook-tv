#!/usr/bin/env python
import boto3
import os
import requests
import sys
from openpyxl import load_workbook
from tools.utils import setup_s3_client, removing_file, download_file, upload_to_s3
from tools.constants import aws_key, aws_secret, s3_bucket, path_to_ffmpeg

global download_path, stream_url
facebook_token = "****"
download_path = '/home/ubuntu/my_live_tv/'
content_file_directory = '/home/ubuntu/my_live_tv/content_file/'
series_counter = 0

new_flag=False

new_content_file_path = '/home/ubuntu/my_live_tv/content_file/****.xlsx'
new_series_counter = 0


def check_if_s3_exist(series_name, episode_to_play, s3_client, s3_bucket, key):
    try:
        s3_client.head_object(Bucket=s3_bucket, Key=key)
        return True
    except Exception, e:
        return False

def read_series_file(_content_file_path):
    data = []
    wb = load_workbook(filename=_content_file_path)
    ws = wb.active
    for i in range(ws.max_row):
        if(i>0):
            data.append({
                            "series_name": ws.cell(row=i+1, column=1).value,
                            "episode_to_play": ws.cell(row=i+1, column=2).value
                        })
    return data

def read_new_series_file(_content_file_path):
    data = []
    wb = load_workbook(filename=_content_file_path)
    ws = wb.active
    for i in range(ws.max_row):
        if(i>0):
            data.append({
                            "series_name": ws.cell(row=i+1, column=1).value,
                            "episode_to_play": ws.cell(row=i+1, column=2).value,
                            "last_episode": ws.cell(row=i+1, column=3).value,
                            "base_link": ws.cell(row=i+1, column=4).value,
                            "description": ws.cell(row=i+1, column=5).value
                        })
    return data

def update_series_file(series_name, _episode_to_play, _content_file_path):
    wb = load_workbook(filename=_content_file_path)
    ws = wb.active
    for i in range(1, ws.max_row):
        if (series_name == ws.cell(row=i, column=1).value):
            ws.cell(row=i, column=2).value = _episode_to_play
    wb.save(_content_file_path)


def download_from_s3(s3_client, key, file_name, s3_bucket):
    try:
        s3_client.download_file(s3_bucket, key, download_path + file_name)
        return file_name
    except Exception, e:
        print(e)
        return None

def get_description(data, all_new_series_names, new_series_counter, counter, episode_to_play, sequential):
    description = "*****".decode('utf-8')
    description += "\n"
    description += data[new_series_counter]['description']
    description += " " + str(episode_to_play) +"\n"
    description += "******".decode('utf-8')
    description += "\n"
    for i in range(5):
        if(i==0 and counter==sequential-1):
            continue
        cnt = new_series_counter + i
        if(cnt >= len(all_new_series_names)):
            cnt = i
        if(i == 0):
            _add = counter + 1
        else:
            _add = 0
        epds = [str(data[cnt]['episode_to_play'] + j) for j in range(_add, sequential) if data[cnt]['episode_to_play'] + j < data[cnt]['last_episode']]
        description += data[cnt]['description'] + " " + ", ".join(epds) +"\n"
    return description

def update_video_description(video_id, facebook_token, data, all_new_series_names, new_series_counter, counter, episode_to_play, sequential):
    description = get_description(data, all_new_series_names, new_series_counter, counter, episode_to_play, sequential)
    r = requests.post(url="https://graph.facebook.com/{0}?access_token={1}&description={2}".format(video_id, facebook_token, description.encode('utf8')))

def broadcast_file_to_live_tv(file_path, stream_url):
    cmd_ = path_to_ffmpeg + "ffmpeg -re -i {0} -c:a aac -strict -2 -bufsize 10000k -c:v copy -f flv \"{1}\"".format(download_path + file_path, stream_url)
    os.system(cmd_)
    return

def broadcast_from_s3(key, stream_url, s3_client, file_name, s3_bucket):
    file_path = download_from_s3(s3_client, key, file_name, s3_bucket)
    if(file_path):
        try:
            broadcast_file_to_live_tv(file_path, stream_url)
            removing_file(download_path + file_path)
            return True
        except Exception, e:
            #FIXME if broadcast failes file remains
            print(e)
            return False
    else:
        return None


def broadcast_from_site(download_link, file_name, series_name, stream_url, s3_client, s3_bucket):
    success = download_file(download_link, download_path + file_name)
    if(success):
        try:
            broadcast_file_to_live_tv(file_name, stream_url)
            upload_to_s3(download_path + file_name, series_name, file_name, s3_client, s3_bucket)
            removing_file(download_path + file_name)
            return True
        except Exception, e:
            #FIXME if broadcast failes file remains
            print(e)
            return False
    else:
        return False


def get_series_info(data, s3_bucket, s3_client):
    all_series_episodes = {}
    episode_to_play = {}
    for x in data:
        series_name = x['series_name']
        episode_to_play[series_name] = x['episode_to_play']
        all_series_episodes[series_name] = []
        results = s3_client.list_objects(Bucket=s3_bucket, Prefix=series_name)
        if(results.get('ResponseMetadata') and results.get('ResponseMetadata').get('HTTPStatusCode') and results.get('ResponseMetadata').get('HTTPStatusCode')==200 and results.get('Contents')):
            all_series_episodes[series_name] = sorted([int(x['Key'][x['Key'].rfind('_')+1:x['Key'].rfind('.')]) for x in results.get('Contents') if x.get('Key')])
    return (all_series_episodes, episode_to_play)

if __name__ == "__main__":
    if(len(sys.argv) < 3):
        sys.exit()
    global new_content_file_path, stream_url, video_id, sequential
    new_content_file_path = content_file_directory + sys.argv[1]
    video_id = sys.argv[2]
    vid_tok = sys.argv[3]
    sequential = int(sys.argv[4])
    stream_url = 'rtmp://*****/{0}?ds=1&a={1}'.format(video_id, vid_tok)
    s3_client = setup_s3_client(aws_key, aws_secret)
    counter = 0
    while True:
        if(True):
            data = read_new_series_file(new_content_file_path)
            all_new_series_names = [x['series_name'] for x in data]
            series_name = all_new_series_names[new_series_counter]
            episode_to_play = data[new_series_counter]['episode_to_play']
            if(episode_to_play>data[new_series_counter]['last_episode']):
                episode_to_play = 1
            file_name =  series_name + "_" + str(episode_to_play) + ".mp4"
            key =  series_name + "/" + file_name
            if(check_if_s3_exist(series_name, episode_to_play, s3_client, s3_bucket, key)):
                try:
                    print("broadcasting from s3")
                    update_video_description(video_id, facebook_token, data, all_new_series_names, new_series_counter, counter, episode_to_play, sequential)
                    broadcast_from_s3(key, stream_url, s3_client, file_name, s3_bucket)
                except Exception, e:
                    print(e)
            else:
                link_to_download = data[new_series_counter]['base_link']
                file_download_name = link_to_download[link_to_download.rfind("/")+1:link_to_download.rfind(".")]+".mp4"
                rest_link = link_to_download[:link_to_download.rfind("/")]
                file_download_name = file_download_name.replace("01","(1)")
                file_download_name = file_download_name.replace("1",str(episode_to_play))
                fixed_link_to_download = rest_link + "/" + file_download_name

                update_video_description(video_id, facebook_token, data, all_new_series_names, new_series_counter, counter, episode_to_play, sequential)
                broadcast_from_site(fixed_link_to_download, series_name + "_" + str(episode_to_play) + ".mp4", series_name, stream_url, s3_client, s3_bucket)
            update_series_file(series_name, episode_to_play + 1, new_content_file_path)
            counter+=1
            if(counter>=sequential):
                new_flag = False
                new_series_counter += 1
                counter = 0
                if(new_series_counter >= len(all_new_series_names)):
                    new_series_counter = 0
