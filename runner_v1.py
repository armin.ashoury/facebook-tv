
import scrapy
import logging
import urllib
import datetime
import os
import schedule
import json
import csv
import requests
import time
import shutil
import boto3
from traceback import format_exc
from subprocess import call, Popen, PIPE
from scrapy.crawler import CrawlerProcess
from scrapy.spiders import Spider
from scrapy.selector import Selector
from openpyxl import load_workbook
import sys

os_ = "Linux"#"Windows"#
stream_on = "facebook"#"youtube"#
download_path = "/home/ubuntu/my_tv/"
path_to_ffmpeg = ""
youtube_url = "******"
facebook_token = "*****"

facebook_page_id = ****
kill_early_sec = 5
aws_key='*****'
aws_secret='*****'
s3_bucket='hulutv'

add_description = ""
'''
    series_file_path csv file with header: name, base_url, episode_to_load, total_episodes
'''
content_file_path = download_path + "content_file.xlsx"

'''
    log_file csv file with header: name, episode_to_broadcast, url, download_success, broadcast_success, time_start, time_ended
'''
log_file = download_path + "my_log.txt"

global names, saved_urls, episodes_to_load, main_thread, stream_url, num_streams, download_logs, parse_urls, data, logwriter, file_row, schedule_message
file_row = None
data = []
main_thread = None
stream_url = None
download_logs = []
schedule_message = None
s3_client = boto3.client('s3',
                        aws_access_key_id=aws_key,
                        aws_secret_access_key=aws_secret)

def log_me(_text):
    logwriter = open(log_file,'a')
    logwriter.write(str(_text) + " \n")
    logwriter.close()

def broadcast_file(file_path, description="", ambient=False, status="LIVE_NOW"):
    global stream_url
    try:
        if(stream_on == "facebook"):
            r = requests.post(url="https://graph.facebook.com/{0}/live_videos?access_token={1}".format(facebook_page_id, facebook_token))
            if(r.status_code==200):
                json_obj=json.loads(r.text)
                stream_url=json_obj['stream_url']
                stream_id=json_obj['id']
                if(ambient):
                    r = requests.post(url="https://graph.facebook.com/{0}?access_token={1}&status={2}&stream_type=AMBIENT&description={3}".format(stream_id, facebook_token, status, description+add_description.encode('utf8')))
                else:
                    r = requests.post(url="https://graph.facebook.com/{0}?access_token={1}&status={2}&description={3}".format(stream_id, facebook_token, status, description+add_description.encode('utf8')))
            else:
                log_me("error in broadcast_file, status = " + str(r.status_code))
        else:
            stream_url = youtube_url
        log_me("broadcasting content")
        cmd_ = path_to_ffmpeg + "ffmpeg -re -i {0} -c:a aac -strict -2 -bufsize 10000k -c:v copy -f flv \"{1}\"".format(download_path + file_path, stream_url)
        log_me(cmd_)
        os.system(cmd_)
        log_me("done broadcasting")
        return True
    except Exception, e:
        log_me("error in stream_url \n" + str(e))
        return False

def post_to_facebook(message):
    r = requests.post(url="https://graph.facebook.com/{0}/feed?message={1}&access_token={2}".format(facebook_page_id, message + add_description.encode('utf8'), facebook_token))

def upload_to_s3(_file_path, _name, _file_name):
    s3_client.upload_file(_file_path, s3_bucket, _name + "/" + _file_name)

def removing_file(_file_path):
    os.remove(_file_path)

def update_logs(name, episode_to_broadcast, url, download_success, broadcast_success, time_start, time_ended):
    # log_file
    return

def check_if_s3_exist(series_name, episode_to_play, s3_client, s3_bucket, key):
    try:
        s3_client.head_object(Bucket=s3_bucket, Key=key)
        return True
    except Exception, e:
        return False

def download_from_s3(s3_client, key, file_name, s3_bucket):
    try:
        s3_client.download_file(s3_bucket, key, download_path + file_name)
        return file_name
    except Exception, e:
        print(e)
        return None

def load_contents_data():
    global file_row, schedule_message
    _names = None
    _urls = None
    _episodes_to_load = None
    _total_episodes = None
    _description = None
    wb = load_workbook(filename=content_file_path)
    ws = wb.active
    for i in range(ws.max_row):
        if(i > 0):
            message = ws.cell(row=i+1, column=5).value + " " + str(ws.cell(row=i+1, column=3).value) + " " + ws.cell(row=i+1, column=6).value + "\n"
            if(schedule_message):
                schedule_message += message
            else:
                schedule_message = message
        if(i == file_row):
            _names=ws.cell(row=i+1, column=1).value
            _urls=ws.cell(row=i+1, column=2).value
            _episodes_to_load=ws.cell(row=i+1, column=3).value
            _total_episodes=ws.cell(row=i+1, column=4).value
            _description=ws.cell(row=i+1, column=5).value

    return (_names, _urls, _episodes_to_load, _total_episodes, _description)


def update_content_file(_cntr, _episode):
    global file_row
    wb = load_workbook(filename=content_file_path)
    ws = wb.active
    for i in range(ws.max_row):
        if(i == file_row):
            ws.cell(row=i+1, column=3).value = _episode
    wb.save(content_file_path)


def download_file(file_url, file_name):
    try:
        urllib.urlretrieve (file_url, download_path + file_name)
        return True
    except Exception, e:
        log_me("can't download {0} from url: {1}".format(file_name, file_url))
        return False


class MySpider(scrapy.Spider):
    name = "exampleSpider"
    global names, saved_urls, episodes_to_load, total_episodes, parse_urls, descriptions, file_row
    if(len(sys.argv) > 1):
        log_me("sys.argv[1]")
        log_me(sys.argv[1])
        file_row = int(sys.argv[1])
    (names, saved_urls, episodes_to_load, total_episodes, descriptions) = load_contents_data()
    start_urls = [saved_urls]#.decode('utf8')
    parse_urls = saved_urls
    def parse(self, response):
        global names, saved_urls, episodes_to_load, total_episodes, main_thread, parse_urls, data, descriptions, file_row
        sel = Selector(response)
        results = sel.xpath("//script[contains(., 'sources')]")
        if(len(results) == 1):
            result = results[0]
            try:
                name = names
                log_me("name")
                log_me(name)
                episode = int(episodes_to_load)
                log_me("episode")
                log_me(episode)
                log_me("total_episodes")
                log_me(total_episodes)
                description = descriptions
                if(episode > int(total_episodes)):
                    log_me("this series ended, go to next content")
                    return
                url = saved_urls
                log_me("url")
                log_me(url)
                script_line = result.extract()
                source_loc=script_line.find("source")
                file_start = script_line[source_loc:].find("file")
                file_end = script_line[(file_start+source_loc):].find("mp4")
                file_url = script_line[(file_start+source_loc+7):(file_start+source_loc+file_end+3)]
                if(episode > 1):
                    if("(1)" in file_url):
                        file_url = file_url.replace("(1)", "("+str(episode)+")")
                    elif("_1" in file_url):
                        file_url = file_url.replace("_1", "_"+str(episode))
                    else:
                        log_me("cant clean file_url: " + str(file_url))
                        return
                file_path = name + "_" + str(episode) + ".mp4"
                log_me("file_url")
                log_me(file_url)
                log_me("file_path")
                log_me(file_path)
                data.append({"name": name, "episode": episode, "file_url": file_url, "file_path": file_path, "episodes_to_load": episodes_to_load, "description": description, "counter": file_row})

            except Exception, e:
                log_me("file not found for {0}, error: {1}".format("NA", e))
        else:
            log_me("len(results) != 1")  # TODO: add url


def run_crawler():
    i = file_row
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    process.crawl(MySpider)
    process.start() # the script will block here until the crawling is finished
    process.join()
    global data
    log_me(data)
    if(True):
        row_ = filter(lambda x: x['counter'] == i, data)
        log_me(len(row_))
        if(len(row_) == 1):
            description_ = row_[0]['description']
            file_path_ = row_[0]['file_path']
            log_me("file_path_")
            log_me(file_path_)
            file_url_ = row_[0]['file_url']
            log_me("file_url_")
            log_me(file_url_)
            name_ = row_[0]['name']
            log_me("name_")
            log_me(name_)
            episodes_to_load_ = row_[0]['episodes_to_load']
            log_me("episodes_to_load_")
            log_me(episodes_to_load_)

            file_name =  name_ + "_" + str(episodes_to_load_) + ".mp4"
            key =  name_ + "/" + file_name
            if(check_if_s3_exist(name_, episodes_to_load_, s3_client, s3_bucket, key)):
                file_path = download_from_s3(s3_client, key, file_name, s3_bucket)
                if(file_path):
                    download_success = True
                else:
                     download_success = False
            else:
                log_me("downloading...")
                download_success = download_file(file_url_, file_path_)
            if(not download_success):
                log_me("download didn't work")
                sys.exit()
            log_me("downloaded")
            episode_ = row_[0]['episode']
            log_me("episode_")
            log_me(episode_)
            log_me("updating content file")
            _counter = row_[0]['counter']
            update_content_file(_counter, int(episodes_to_load_) + 1)
            log_me("uploading file")
            upload_to_s3(download_path + file_path_, name_, name_ + "_" + str(episode_) + ".mp4")
            description = description_ + " " + str(episodes_to_load_)
            log_me("description")
            log_me(description.encode('utf8'))
            broadcast_success = broadcast_file(file_path_, description=description.encode('utf8'))
            log_me("removing file" + file_path_)
            removing_file(file_path_)
            log_me("finished video")
        else:
            log_me("no file for " + str(i))
    log_me("done...")

if __name__ == "__main__":
    global file_row
    for file in os.listdir(download_path):
        if file.endswith(".mp4"):
            log_me("removing mp4 file" + file)
            try:
                removing_file(download_path+file)
            except Exception, e:
                log_me("error removing mp4 file" + str(e))
    if(len(sys.argv) > 1):
        try:
            run_crawler()
        except:
            log_me(format_exc())
    else:
        if(schedule_message is not None):
            log_me("schedule_message")
            log_me(schedule_message.encode('utf8'))
            post_to_facebook(schedule_message.encode('utf8'))
