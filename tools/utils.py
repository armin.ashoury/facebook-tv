
import boto3
import requests
import json
import os
import urllib
from openpyxl import load_workbook
from tools.constants import log_file, youtube_url, path_to_ffmpeg, s3_bucket


def setup_s3_client(aws_key, aws_secret):
    s3_client = boto3.client('s3',
                        aws_access_key_id=aws_key,
                        aws_secret_access_key=aws_secret)
    return s3_client

def log_me(_text):
    logwriter = open(log_file,'a')
    logwriter.write(str(_text) + " \n")
    logwriter.close()

def broadcast_file(file_path, _facebook_page_id, facebook_token, broadcast_file_path, _stream_on, description="", ambient=False, status="LIVE_NOW"):
    global stream_url
    try:
        if(_stream_on == "facebook"):
            r = requests.post(url="https://graph.facebook.com/{0}/live_videos?access_token={1}".format(_facebook_page_id, _facebook_token))
            if(r.status_code==200):
                json_obj=json.loads(r.text)
                stream_url=json_obj['stream_url']
                stream_id=json_obj['id']
                if(ambient):
                    r = requests.post(url="https://graph.facebook.com/{0}?access_token={1}&status={2}&stream_type=AMBIENT&description={3}".format(stream_id, _facebook_token, status, description))
                else:
                    r = requests.post(url="https://graph.facebook.com/{0}?access_token={1}&status={2}&description={3}".format(stream_id, _facebook_token, status, description))
            else:
                log_me("error in broadcast_file, status = " + str(r.status_code))
        else:
            stream_url = youtube_url
        log_me("broadcasting content")
        cmd_ = path_to_ffmpeg + "ffmpeg -re -i {0} -c:a aac -strict -2 -bufsize 10000k -c:v copy -f flv \"{1}\"".format(broadcast_file_path, stream_url)
        log_me(cmd_)
        os.system(cmd_)
        log_me("done broadcasting")
        return True
    except Exception, e:
        log_me("error in stream_url \n" + str(e))
        return False


def post_to_facebook(message, _facebook_page_id, _facebook_token):
    r = requests.post(url="https://graph.facebook.com/{0}/feed?message={1}&access_token={2}".format(_facebook_page_id, message, _facebook_token))


def upload_to_s3(_file_path, _name, _file_name, s3_client, s3_bucket):
    s3_client.upload_file(_file_path, s3_bucket, _name + "/" + _file_name)


def removing_file(_file_path):
    os.remove(_file_path)


def load_contents_data(_file_row, _schedule_message, _content_file_path):
    _names = None
    _urls = None
    _episodes_to_load = None
    _total_episodes = None
    _description = None
    wb = load_workbook(filename=_content_file_path)
    ws = wb.active
    for i in range(ws.max_row):
        if(i > 0):
            message = ws.cell(row=i+1, column=5).value + " " + str(ws.cell(row=i+1, column=3).value) + " " + ws.cell(row=i+1, column=6).value + "\n"
            if(_schedule_message):
                _schedule_message += message
            else:
                _schedule_message = message
        if(i == _file_row):
            _names=ws.cell(row=i+1, column=1).value
            _urls=ws.cell(row=i+1, column=2).value
            _episodes_to_load=ws.cell(row=i+1, column=3).value
            _total_episodes=ws.cell(row=i+1, column=4).value
            _description=ws.cell(row=i+1, column=5).value

    return (_names, _urls, _episodes_to_load, _total_episodes, _description)

def update_content_file(_cntr, _episode, _file_row, _content_file_path):
    wb = load_workbook(filename=_content_file_path)
    ws = wb.active
    for i in range(ws.max_row):
        if(i == _file_row):
            ws.cell(row=i+1, column=3).value = _episode
    wb.save(_content_file_path)


def download_file(file_url, _file_path):
    try:
        urllib.urlretrieve (file_url, _file_path)
        return True
    except Exception, e:
        print(e)
        return False
